package omr_capsys;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import omr_capsys.Display;

public class detectCircles {

	 static private Mat newImage = new Mat();
	
	 public static Mat getImage()
	    {
	        return newImage;
	    }
    public static Mat detect(Mat croppedImg){
		      
		      Imgproc.cvtColor(croppedImg, newImage, Imgproc.COLOR_BGR2GRAY);
		      // Imgproc.GaussianBlur(newImage, newImage, new Size(9, 9) ,2);
		        Mat circles= new Mat();
		        Imgproc.HoughCircles(newImage, circles, Imgproc.CV_HOUGH_GRADIENT,1, 25,100,10,10,15);
		        Imgproc.cvtColor(newImage, newImage, Imgproc.COLOR_GRAY2RGB);

		        for (int i = 0; i < circles.cols(); i++) {
		        	double[] vector = circles.get(0, i);
		            Point pt1 = new Point(vector[0], vector[1]);
		            int r = (int) vector[2];
		          // System.out.println(r);
		            Scalar color  = new Scalar(0,255,0);
		            Core.circle(newImage, pt1, 1, color,1);
		            Core.circle(newImage, pt1, r, color,2);   
		    	}
		        
		       // Display.displayImage(newImage);
		       Highgui.imwrite("resources/circleDetected.jpg",newImage);
		        
		        return circles;
	}
}
	   
