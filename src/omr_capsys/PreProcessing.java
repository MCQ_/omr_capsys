package omr_capsys;


import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;


/**
 * 
 */
public class PreProcessing
{
	
	
    public static Mat convertToGrayRGB(Mat brgLoadedImage)
    {
        Mat resultImage = new Mat(brgLoadedImage.size(), CvType.CV_8U);
        Mat tempImg = brgLoadedImage.clone();
        

        Imgproc.cvtColor(brgLoadedImage, tempImg, Imgproc.COLOR_BGR2RGB);
    	Imgproc.cvtColor(tempImg, tempImg, Imgproc.COLOR_RGB2GRAY);
        Imgproc.adaptiveThreshold(tempImg, tempImg, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 75, 10); 

        tempImg.convertTo(resultImage, CvType.CV_8U);

        brgLoadedImage.release();
        brgLoadedImage = null;
        tempImg.release();
        tempImg = null;
        
        return resultImage;
    }   
    
 }
   
   

