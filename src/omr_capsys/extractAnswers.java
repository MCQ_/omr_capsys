package omr_capsys;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Vector;
import java.util.function.UnaryOperator;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.features2d.DMatch;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.CvType;

public class extractAnswers {

	private static char getletter(int col) {

		int ind = col % 4;
		char letter = 'X';
		switch (ind) {

		case 0:
			letter = 'A';
			break;
		case 1:
			letter = 'B';
			break;
		case 2:
			letter = 'C';
			break;
		case 3:
			letter = 'D';
			break;
		}

		return letter;
	}

	private static int getRnumber(int row){
		
		int realNUM =(row+1)%10;
		return realNUM;
	}
	
	public static List<Map.Entry<Integer, Character>> sortCircles(Mat circles, Mat inputImage, Mat cImage) {

		List<Double> rows = new ArrayList<>();
		List<Double> columns = new ArrayList<>();

		Mat bubble = circles.clone();
		for (int i = 0; i < circles.cols(); i++) {
			boolean found = false;
			double[] data = circles.get(0, i);
			double r = data[2];
			double x = data[0];
			double y = data[1];
			for (int j = 0; j < rows.size(); j++) {
				double y2 = rows.get(j);
				if (y - r < y2 && y + r > y2) {
					bubble.col(j).push_back(circles.col(j));
					found = true;
					break;
				}
			}
			if (!found) {
				rows.add(y);
				bubble.col(i).push_back(circles.col(i));
			}
			found = false;
			for (int j = 0; j < columns.size(); j++) {
				double x2 = columns.get(j);
				if (x - r < x2 && x + r > x2) {
					bubble.col(j).push_back(circles.col(j));
					found = true;
					break;
				}
			}
			if (!found) {
				columns.add(x);
				bubble.col(i).push_back(circles.col(i));
			}
		}

		// sort x coord // order from left to right
		// System.out.println("befor:"+rows);
		Collections.sort(rows, new Comparator<Double>() {

			@Override
			public int compare(Double d1, Double d2) {
				// TODO Auto-generated method stub
				int result = Double.compare(d1, d2);
				return result;
			}
		});
	//	System.out.println("after:" + rows);

		// sort x coord // order from left to right
		// System.out.println("befor:"+columns);
		Collections.sort(columns, new Comparator<Double>() {

			@Override
			public int compare(Double d1, Double d2) {
				// TODO Auto-generated method stub
				int result = Double.compare(d1, d2);
				return result;
			}
		});
	//	System.out.println("after:" + columns);

		List<Double> bubbleFsort = new ArrayList<>();
		for (int i = 0; i < bubble.cols(); i++)
			bubbleFsort.add(bubble.col(i).get(0, 0)[0]);

		//System.out.println("befor:" + bubbleFsort);

		Collections.sort(bubbleFsort, new Comparator<Double>() {

			@Override
			public int compare(Double d1, Double d2) {
				// TODO Auto-generated method stub
				int result = Double.compare(d1, d2);
				return result;
			}
		});

		//System.out.println("after:" + bubbleFsort);
		int npixels = (int) (bubble.col(0).total() * bubble.col(0).elemSize());
		double[] sortedXvector = new double[npixels];
		for (int i = 0; i < bubbleFsort.size(); i++) {
			double e = bubbleFsort.get(i);
			double yUnchanged = bubble.col(i).get(0, 0)[1];
			double rUnchanged = bubble.col(i).get(0, 0)[2];
			sortedXvector[0] = e;
			sortedXvector[1] = yUnchanged;
			sortedXvector[2] = rUnchanged;
			bubble.col(i).put(0, 0, sortedXvector);

		}
		
		/* Initiate the final answer sheet form 
		As shown it is List of Map.Entry<Integer, Character>
		key: Question number
		value: the highlighted answer character*/
		
		List<Map.Entry<Integer, Character>> markedSheet = new ArrayList<>();
		int indBlock = 0;
		int realInd = 0;
		int  ROLLnum =0;
		int counter;

		for (int i = 0; i < columns.size(); i++) {
			counter=1;
			char Letter = getletter(i);
			double max = 0.6;
			int sep = 4;
			double x = columns.get(i);
			int nextBlock = 0;
			for (int j = 0; j < rows.size(); j++) {
				int ind = -1;
				double[] circleC = bubble.col(j).get(0, 0);
				int r = (int) circleC[2];
				double y = rows.get(j);
				Point C = new Point(x, y);
				Core.circle(inputImage, C, r, new Scalar(0, 0, 255), 2);
				Rect rect = new Rect((int) x - r, (int) y - r, 2 * r, 2 * r);
				Mat Image1 = new Mat();
				Imgproc.cvtColor(cImage, Image1, Imgproc.COLOR_RGB2GRAY);
				Mat subMat = new Mat(Image1, rect);
				double p = (double) Core.countNonZero(subMat) / (subMat.size().width * subMat.size().height);
				nextBlock = rows.size() * indBlock;
				realInd = (j + 1) + nextBlock;
				if (p >= 0.65) {
					max = p;
					ind = j;
					ROLLnum=getRnumber(j);
				}

				if (ind == -1)
					System.out.print("");
				else 
				{
					if(rows.size() < 20){    //condition need to be  generalized, not completed
					
						if(counter <2){
							ROLLmarkedSheet.addLast(ROLLnum);
							counter++; 
							}
						else
							System.out.println("Incorrect Roll no. :: repeated entry in columne: "+(i+1));
				    
					}	
					else{					
					   Map.Entry<Integer, Character> finalA = new AbstractMap.SimpleEntry<>(realInd, Letter);
					   markedSheet.add(finalA);
					}
				}

			}
			if (i + 1 == sep * (indBlock + 1)) {
				indBlock++;
			}
		}
		Highgui.imwrite("resources/row.jpg", inputImage);
		return markedSheet;

	}

     private static LinkedList<Integer> ROLLmarkedSheet=new LinkedList<>();

     public static String getROLLmarkedSheet(){  
	 String ROLL=ROLLmarkedSheet.toString();
    	return ROLL;
     }


    public static  List<Map.Entry<Integer, Character>> OrderedMarkedSheet( List<Map.Entry<Integer, Character>> markedSheet){
  	 
    	/*Check repeated entry:: 
    	cases to be handled:
    	 1. two successive questions, second one ignored ( Q3, Q4), 
    	 2. if( Q1 and Q21) have repeated answers, second ignored
    	 */
    	
    	for(int k=0; k<markedSheet.size();k++){
    		for(int j= k+1; j<markedSheet.size();j++){
     		 if(markedSheet.get(k).getKey() == markedSheet.get(j).getKey()){
     			 markedSheet.remove(j);
     			 System.out.println("repeated entry; question: "+markedSheet.get(k).getKey());
     			 markedSheet.remove(k);
     			 
     		 }
    	 }
    }
       	
	        //Order Question Number//
			List<Integer> fQnum = new ArrayList<>();
			for (int i = 0; i < markedSheet.size(); i++)
				fQnum.add(markedSheet.get(i).getKey());

			Collections.sort(fQnum, new Comparator<Integer>() {

				@Override
				public int compare(Integer qNum1, Integer qNum2) {
					// TODO Auto-generated method stub
					int result = Integer.compare(qNum1, qNum2);
					return result;
				}
			});
		    
			// Get the opposite letter of the ordered questions//
			List<Character> fAnswerLetter = new ArrayList<>();
			for (int i = 0; i < fQnum.size(); i++) {
				int qNum = fQnum.get(i);
				for (int k = 0; k < markedSheet.size(); k++) {
					Map.Entry<Integer, Character> perColD = markedSheet.get(k);

					if (qNum == perColD.getKey()) {
						char letter = perColD.getValue();
						fAnswerLetter.add(letter);
					}
				}
			}
			//Set the final form of the map of the answer sheet 
			for (int k = 0; k < markedSheet.size(); k++) {
				int Qnum = fQnum.get(k);
				char answerLetter = fAnswerLetter.get(k);
				Map.Entry<Integer, Character> answerOrderd = new AbstractMap.SimpleEntry<>(Qnum, answerLetter);
				markedSheet.set(k, answerOrderd);
			}

return markedSheet;
}
}



