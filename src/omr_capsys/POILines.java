package omr_capsys;


import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

// **
/**this method detects corners of
tilted image to be cropped into the desired area **
POI = Point Of Intersection
**/
import omr_capsys.detectCircles;
import omr_capsys.PreProcessing;

public class POILines {

	static Mat newImage = new Mat();
    static Scalar color = new Scalar ( 0 ,255,255);
	private static Point computeIntersect(double [] a, double[] b){
		
	    double x1 = a[0], y1 = a[1], x2 = a[2], y2 = a[3];
	    double x3 = b[0], y3 = b[1], x4 = b[2], y4 = b[3];
	    float d = (float) (((float)(x1-x2) * (y3-y4)) - ((y1-y2) * (x3-x4)));
	    if (d != 0)
	    {
	        Point pt = new Point();
	        pt.x = ((x1*y2 - y1*x2) * (x3-x4) - (x1-x2) * (x3*y4 - y3*x4)) / d;
	        pt.y = ((x1*y2 - y1*x2) * (y3-y4) - (y1-y2) * (x3*y4 - y3*x4)) / d;
	        return pt;
	    }
	    else
	    {
	    	Point pt = new Point(-1,-1);
	        return pt;
	        }
		
	}
	public static List<List<Point>> Hough(Mat thresholdImage){
        Imgproc.cvtColor(thresholdImage, newImage, Imgproc.COLOR_GRAY2RGB);
        Mat lines= new Mat();
        Imgproc.HoughLinesP(thresholdImage, lines, 1, Math.PI/180, 80, 890, 10);
        for (int i = 0; i < lines.cols(); i++) {
            double[] vector = lines.get(0, i);
            Point pt1 = new Point(vector[0], vector[1]);
            //System.out.println(pt1);
            Point pt2 = new Point(vector[2], vector[3]);
            Scalar color  = new Scalar(0,255,0);
           Core.line(newImage, pt1, pt2, color,2);   //line(image, start, end, color,thickness)//
            
    	}
      // Display.displayImage(newImage);

 /// intersection ///
		List <Point> corners = new ArrayList <Point> ();
        for (int i = 0; i < lines.cols(); i++)
        {
            for (int j = i+1; j < lines.cols(); j++)
            {
                 Point pt = new Point() ;
                 pt = computeIntersect(lines.get(0, i), lines.get(0, j));
                 if (pt.x >= 0 && pt.y >= 0 && pt.x < thresholdImage.cols() && pt.y < thresholdImage.rows())
                    corners.add(pt);
            }
        }
    //System.out.println(corners.size());
    //System.out.println(corners);

     List <List<Point>> sections = new ArrayList <List<Point>> ();

    List <Point> TOPsection = new ArrayList <Point> ();
	List <Point> BOTTOMsection = new ArrayList <Point> ();
	
	BOTTOMsection.add(corners.get(4));
	BOTTOMsection.add(corners.get(5));
	BOTTOMsection.add(corners.get(6));
	BOTTOMsection.add(corners.get(7));
	
	TOPsection.add(corners.get(0));
	TOPsection.add(corners.get(1));
	TOPsection.add(corners.get(2));
	TOPsection.add(corners.get(3));

    sections.add(BOTTOMsection);
    sections.add(TOPsection);

        return sections;
         
     }
	


	
	 public static Mat sortCorners(List <Point> corners, Point center){

		 List <Point> top = new ArrayList <Point> ();
		List <Point> bottom = new ArrayList <Point> ();
		for (int i = 0; i < corners.size(); i++)  
	    {  
		     if (corners.get(i).y < center.y)
                top.add(corners.get(i)); 
		    else  
		        bottom.add(corners.get(i));  
		 }  
		
		int minX = Integer.MAX_VALUE;
		int minY = Integer.MAX_VALUE;
		
		for ( Point t : top ) {
		  final int xt = (int)t.x;
		  final int yt = (int)t.y;
		  if ( xt < minX ) 
		    minX = xt;
		  if ( yt < minY ) 
		    minY = yt;
		}
		for ( Point b : bottom ) {
			  final int xb = (int)b.x;
			  final int yb = (int)b.y;
			  if ( xb < minX ) 
			    minX = xb;
			  if ( yb < minY ) 
			    minY = yb;
			}
		Collections.sort( top, new Comparator<Point>() {
			
			   @Override
		       public int compare(Point x1, Point x2) {
				 // TODO Auto-generated method stub
		         int result = Double.compare(x1.x, x2.x);
		         return result;
		      }
		    });

		Collections.sort( bottom, new Comparator<Point>() {
			
			   @Override
		       public int compare(Point x1, Point x2) {
				 // TODO Auto-generated method stub
		         int result = Double.compare(x1.x, x2.x);
		         return result;
		      }
		    });
		
		Point  tL = top.get(0);
		Point  tR = top.get(top.size()-1);
		Point  bL = bottom.get(0);
		Point  bR = bottom.get(bottom.size()-1);
         
//        Core.circle(newImage, new Point(tL.x, tL.y), 10, color, 2); //point is colored yellow
//        Core.circle(newImage, new Point(tR.x, tR.y), 10, color, 2); 
//        Core.circle(newImage, new Point(bL.x, bL.y), 10, color, 2); 
//        Core.circle(newImage, new Point(bR.x, bR.y), 10, color, 2); 
//        System.out.println(corners.get(3));
//        System.out.println(corners.get(2));
//        System.out.println(corners.get(1));
//        System.out.println(corners.get(0));

        MatOfPoint2f src = new MatOfPoint2f(
        		tL,
        		tR,
        		bR,
        		bL
        	);
       double widthA = Math.sqrt(Math.pow( (bR.x - bL.x) , 2) + Math.pow( (bR.y - bL.y) , 2) );
       double widthB = Math.sqrt(Math.pow( (tR.x - tL.x) , 2) + Math.pow( (tR.y - bL.y) , 2) );
       double maxWidth = Math.max(widthA, widthB);
       double heightA = Math.sqrt(Math.pow( (tR.x - bR.x) , 2) + Math.pow( (tR.y - bR.y) , 2) );
       double heightB = Math.sqrt(Math.pow( (tL.x - bL.x) , 2) + Math.pow( (tL.y - bL.y) , 2) );
       double maxHeight = Math.max(heightA, heightB);
       
        MatOfPoint2f dst = new MatOfPoint2f(
        	    new Point(0,0), // awt has a Point class too, so needs canonical name here
        	    new Point(maxWidth-1,0),
        	    new Point(maxWidth-1,maxHeight-1),
        	    new Point(0,maxHeight-1)
        	);

      //  System.out.println(src.dump());

        Mat transmtx = Imgproc.getPerspectiveTransform(src, dst);
       // System.out.println(transmtx.dump());
        Mat inputImage = newImage.clone();
        Mat result = new Mat(inputImage.size(),CvType.CV_32FC2 );
        Imgproc.warpPerspective(inputImage, result, transmtx,new Size(maxWidth,maxHeight));
//        Display.displayImage(result);
        return result;
	 }
	 

      
    public static List<Point> massOfCenter(Mat image){

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(image, contours, hierarchy,
                Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_NONE);
        
     // find the largest contour
        double largestArea = 0;
        double smallerArea = 0;
        int contourIndx = 0;
        int contourIndxS = 0;

        Scalar color = new Scalar ( 0 ,255,0);

        for( int i = 0; i< contours.size(); i++ )
        {
            double area = Imgproc.contourArea( contours.get(i),false); 
            if(area > largestArea)
            {
                largestArea = area;
                contourIndx = i;  
            }
            if(area*3>largestArea){
            	smallerArea=area;
            	contourIndxS=i;
            	}
            }

        MatOfPoint cnt1 = contours.get(contourIndx);
        Moments M1 = Imgproc.moments(cnt1);
        
        MatOfPoint cnt2 = contours.get(contourIndxS);
        Moments M2 = Imgproc.moments(cnt2);
        
        double Cx1 = (int) (M1.get_m10() / M1.get_m00());
        double Cy1 = (int) (M1.get_m01() / M1.get_m00());
        
        double Cx2 = (int) (M2.get_m10() / M2.get_m00());
        double Cy2 = (int) (M2.get_m01() / M2.get_m00());
        
		List <Point> cen = new ArrayList <Point> ();

        cen.add(new Point (Cx1,Cy1));
        cen.add(new Point (Cx2,Cy2));
		return cen;

        }
    
}