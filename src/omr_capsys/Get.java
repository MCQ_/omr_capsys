package omr_capsys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;

public class Get {
	
    
  private static  List <Map.Entry<Integer, Character> > finalSheet = new ArrayList<>();
  private static String ROLLfinal;
  
       public static List <Map.Entry<Integer, Character>> getFinalMarkedSheet(Mat refBitwise, Mat sheetBitWise){
        
                int result= checkOrientation.Isfliped(refBitwise, sheetBitWise);
                if(result==1){
                	
                    System.out.println("Matched Images..");
                    List<List<Point>> Corners = new ArrayList<List<Point>>();
                    Corners = POILines.Hough(sheetBitWise);
                    List<Point> center = POILines.massOfCenter(sheetBitWise);
                    
                    Mat AnswersRegion = POILines.sortCorners(Corners.get(0), center.get(0));
                    Mat Answerscircles= detectCircles.detect(AnswersRegion);
                    Mat _srcimage = detectCircles.getImage();
                    
                    List <Map.Entry<Integer, Character> > markedSheet = new ArrayList<>();
                    markedSheet = extractAnswers.sortCircles(Answerscircles,AnswersRegion,_srcimage);
                    finalSheet = extractAnswers.OrderedMarkedSheet(markedSheet);

                    
                    Mat StudentDataRegion = POILines.sortCorners(Corners.get(1), center.get(1));
                    Mat StudentDatacircles= detectCircles.detect(StudentDataRegion);
                    Mat _srcimage2 = detectCircles.getImage();
                    
                    List <Map.Entry<Integer, Character> > finalSheet2 = new ArrayList<>();
                    finalSheet2 = extractAnswers.sortCircles(StudentDatacircles,StudentDataRegion,_srcimage2);
                     ROLLfinal  = extractAnswers.getROLLmarkedSheet();


              }    
                
                if(result==2){

                    System.out.println("Matched Images.// FLIPPEd.");
                    Core.flip(sheetBitWise, sheetBitWise, 0);
                    List<List<Point>> Corners = new ArrayList<List<Point>>();
                    Corners = POILines.Hough(sheetBitWise);
                    List<Point> center = POILines.massOfCenter(sheetBitWise);
                    
                    Mat AnswersRegion = POILines.sortCorners(Corners.get(1), center.get(0));
                    Mat Answerscircles= detectCircles.detect(AnswersRegion);
                    Mat _srcimage = detectCircles.getImage();
                    
                    List <Map.Entry<Integer, Character> > markedSheet = new ArrayList<>();
                    List <Map.Entry<Integer, Character> > finalSheet = new ArrayList<>();

                    markedSheet = extractAnswers.sortCircles(Answerscircles,AnswersRegion,_srcimage);
                    finalSheet = extractAnswers.OrderedMarkedSheet(markedSheet);
                    
                    Mat StudentDataRegion = POILines.sortCorners(Corners.get(0), center.get(1));
                    Mat StudentDatacircles= detectCircles.detect(StudentDataRegion);
                    Mat _srcimage2 = detectCircles.getImage();
                    
                    List <Map.Entry<Integer, Character> > finalSheet2 = new ArrayList<>();
                    finalSheet2 = extractAnswers.sortCircles(StudentDatacircles,StudentDataRegion,_srcimage2);

               }
                
              if(result==0){
            	  System.out.println("Something wrong, MARKER NOT FOUND");
            	  } 
              
              return finalSheet;
    }
   
        


   public static String getRollnum(){
	   return  ROLLfinal;
   }

}

