package omr_capsys;
import java.util.ArrayList;
import java.util.LinkedList;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.features2d.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.util.LinkedList;
import java.util.List;


public class featuredetector {

	public static boolean IsMatch(Mat referencImage, Mat capturedImage){
			
		Rect ROI = new Rect(0, 0, capturedImage.cols(), capturedImage.rows()/6);
		capturedImage= new Mat(capturedImage,ROI);
		
	//	Display.displayImage(capturedImage);
//initialization		 
        FeatureDetector OMR_detector = FeatureDetector.create(FeatureDetector.SURF);
        DescriptorExtractor OMR_descriptor = DescriptorExtractor.create(DescriptorExtractor.SURF);
        DescriptorMatcher OMR_matcher = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);
//first
        
        MatOfKeyPoint referenceKeyPoints = new MatOfKeyPoint();
        MatOfKeyPoint referenceDescriptors = new MatOfKeyPoint();
        OMR_detector.detect(referencImage, referenceKeyPoints);
        OMR_descriptor.compute(referencImage, referenceKeyPoints, referenceDescriptors);
//second
        MatOfKeyPoint capturedeKeyPoints = new MatOfKeyPoint();
        MatOfKeyPoint capturedDescriptors = new MatOfKeyPoint();
        OMR_detector.detect(capturedImage, capturedeKeyPoints);
        OMR_descriptor.compute(capturedImage, capturedeKeyPoints, capturedDescriptors);
//check        
        if(referenceDescriptors.empty() ||capturedDescriptors.empty() || capturedeKeyPoints.empty()|| referenceKeyPoints.empty() ){
        	System.out.println("Empty mat/s..");
        }
//match      
        
        Mat matchImage =new Mat();
        Scalar matchColor = new Scalar(0, 255, 0);
        Scalar newKeypointColor = new Scalar(0,0, 255); 
        
        List<MatOfDMatch> matches = new  ArrayList<MatOfDMatch> ();
       // MatOfDMatch matches =new MatOfDMatch();
        OMR_matcher.knnMatch(capturedDescriptors, referenceDescriptors, matches,2); //the best two matches
       
        LinkedList<DMatch> goodMatches= new LinkedList<DMatch>();

        double ratio=0.4;
        
 //filtering       
        for(int i=0; i< matches.size();i++)
        {
       	     MatOfDMatch matofDMatch = matches.get(i);
             DMatch[] dmatcharray = matofDMatch.toArray();
             DMatch m1 = dmatcharray[0];
             DMatch m2 = dmatcharray[1];
//            System.out.println(m1.distance+" "+ m2.distance);
             if (m1.distance <=m2.distance*ratio) 
            	 goodMatches.addLast(m1);
        }
        MatOfDMatch mat = new MatOfDMatch();
        mat.fromList(goodMatches);

//results        
        boolean result;
        
        if(goodMatches.size() >= 15) 
        	result =true;
        else
        	result =false;

//drawing
        if(result){
        MatOfDMatch MatgoodMatches = new MatOfDMatch();
        MatgoodMatches.fromList(goodMatches);
        //System.out.println(MatgoodMatches.size());
      
        Features2d.drawMatches(capturedImage, capturedeKeyPoints, referencImage, referenceKeyPoints, MatgoodMatches, matchImage, Scalar.all(-1), Scalar.all(-1), new MatOfByte(), 2);
        //Display.displayImage(matchImage);
        }
	
	return result;
	}
}
