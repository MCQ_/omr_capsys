package omr_capsys;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Vector;
import java.util.function.UnaryOperator;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.features2d.DMatch;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.CvType;

public class AllCircles {
	
    
    
	public static void sortCircles(Mat circles, Mat inputImage,Mat cImage ){
		  
		  
	      List <Double> rows = new ArrayList<>();
	      List <Double> columns = new ArrayList<>();

          Mat bubble = circles.clone();
		  double averR = 0;

//		  System.out.println("bubble"+bubble.rows());
	      for(int i=0;i<circles.cols();i++){  
			    boolean found = false;
			    double[] data = circles.get(0, i);
			    double r =  data[2];
			    averR+=r;
         	    double x = data[0];
     		    double y = data[1];
 		        for(int j=0;j<rows.size();j++){
			        double y2 = rows.get(j);
			        if(y - r < y2 && y + r > y2){
			          bubble.col(j).push_back(circles.col(j));
     		          found = true;
        	          break;
 		            }
		         } 
			    if(!found){
			    	  rows.add(y);
                      bubble.col(i).push_back(circles.col(i));
			    }
			    found = false;
			    for(int j=0;j<columns.size();j++){
			        double x2 = columns.get(j);
			        if(x - r < x2 && x + r > x2){
			          bubble.col(j).push_back(circles.col(j));
     		          found = true;
        	          break;
 		            }
		         } 
			    if(!found){
				      columns.add(x);
                      bubble.col(i).push_back(circles.col(i));
			    }
           }
	      //averR /= circles.cols();
	      
//        sort x coord // order from left to right 
//        System.out.println("befor:"+rows);
          Collections.sort(rows, new Comparator<Double>() {
  			
			   @Override
		       public int compare(Double d1, Double d2) {
				 // TODO Auto-generated method stub
		         int result = Double.compare(d1, d2);
		         return result;
		      }
		    });
          System.out.println("after:"+rows);
          
//         sort x coord // order from left to right 
//        System.out.println("befor:"+columns);
          Collections.sort(columns, new Comparator<Double>() {
  			
			   @Override
		       public int compare(Double d1, Double d2) {
				 // TODO Auto-generated method stub
		         int result = Double.compare(d1, d2);
		         return result;
		      }
		    });
          System.out.println("after:"+columns);

          
          List<Double> bubbleFsort = new ArrayList<>();
          for (int i = 0; i < bubble.cols(); i++) 
			bubbleFsort.add(bubble.col(i).get(0, 0)[0]);
		   
          System.out.println("befor:"+bubbleFsort);

          Collections.sort(bubbleFsort, new Comparator<Double>() {
    			
			   @Override
		       public int compare(Double d1, Double d2) {
				 // TODO Auto-generated method stub
		         int result = Double.compare(d1, d2);
		         return result;
		      }
		    });
          
          System.out.println("after:"+bubbleFsort);
          int npixels = (int) (bubble.col(0).total()* bubble.col(0).elemSize());
          double[] sortedXvector = new double[npixels];
          for (int i = 0; i < bubbleFsort.size(); i++){
        	  double e = bubbleFsort.get(i);
        	  double yUnchanged = bubble.col(i).get(0, 0)[1];
        	  double rUnchanged = bubble.col(i).get(0, 0)[2];
        	  sortedXvector[0] = e;
        	  sortedXvector[1] = yUnchanged;
        	  sortedXvector[2] = rUnchanged;
        	  bubble.col(i).put(0, 0,sortedXvector );

          }
          for (int i = 0; i < columns.size(); i++) {
			double max = 0;
			int ind =-1;
	        double x = columns.get(i);
	        System.out.println(columns.size());
			for (int j = 0; j < rows.size()  ; j++) {
				 int rowInd = i+1;
				 int  sep= 3;
				 int dispInd = 1;
				 double [] circleC  = bubble.col(j).get(0, 0);
     			 int r =  (int)circleC[2];
				 double y = rows.get(j);
	     		 Point C = new Point(x,y);
	     		 int averRad = (int)averR;
	     	     Core.circle(inputImage, C, r, new Scalar(0,0,255), 2);
	     		 Rect rect = new Rect((int) x-r,(int) y-r,2*r,2*r);
	     		 Mat Image1 = new Mat();
			     Imgproc.cvtColor(cImage, Image1, Imgproc.COLOR_RGB2GRAY);    
                  Mat subMat =new Mat(Image1,rect);

     		     double p =(double)Core.countNonZero(subMat) / (subMat.size().width*subMat.size().height);  
    		     if(p>=0.5 && p>max){  
     			    max = p;  
     			    ind = j;  
     			   }  
			}
		     if(ind==-1)System.out.println(i+1 +":" );  
		     else
		    	 {
		    	 System.out.print(i+1 +":");  
		    	 System.out.println("A");  
		    	 }
		     
		}
       Display.displayImage(inputImage);
       Highgui.imwrite("resources/rows.jpg",inputImage);
 
	}	
}

