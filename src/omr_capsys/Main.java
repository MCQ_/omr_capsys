package omr_capsys;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.*;

import omr_capsys.Display;
import omr_capsys.POILines;
import omr_capsys.PreProcessing;
import omr_capsys.detectCircles;

/**
 *
 * Main class, the program starts here ...
 *
 */

public class Main{
	static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
    static Mat ref = Highgui.imread("resources/ref3.jpg"); 
    static Mat sheet = Highgui.imread("resources/template3*.jpg"); 
//    static Mat Model = Highgui.imread("resources/template30.jpg"); 

    
    static Mat refGrayRGB = new Mat();
    static Mat sheetGrayRGB = new Mat();
//    static Mat modelGrayRGB = new Mat();

   
    static Mat refBitwise = new Mat();
    static Mat sheetBitWise = new Mat();
//    static Mat modelBitWise = new Mat();

    static Scalar color  = new Scalar(255,0,0);

    public static void main(String[] args) 
    
    {  	
    	
        refGrayRGB= PreProcessing.convertToGrayRGB(ref);
        sheetGrayRGB= PreProcessing.convertToGrayRGB(sheet);
  //      modelGrayRGB= PreProcessing.convertToGrayRGB(Model);


        Core.bitwise_not(refGrayRGB,refBitwise);
        Core.bitwise_not(sheetGrayRGB,sheetBitWise);
 //       Core.bitwise_not(modelGrayRGB,modelBitWise);
        

        List <Map.Entry<Integer, Character>> FinalMarkedSheet= new ArrayList<>();
//        List <Map.Entry<Integer, Character>> ModelMarkedSheet= new ArrayList<>();

        FinalMarkedSheet = Get.getFinalMarkedSheet(refBitwise, sheetBitWise);
        String Rollnum = Get.getRollnum();  
        System.out.println("Final Answers Sheet: " + FinalMarkedSheet);
        System.out.println("Roll no. : " + Rollnum);

        
//        ModelMarkedSheet = Get.getFinalMarkedSheet(refBitwise, modelBitWise);
//        System.out.println("Model Answers Sheet: " + ModelMarkedSheet);


        

}
  
}